import java.util.Scanner;

import org.campus02.shape.FrameShape;
import org.campus02.shape.MoveUp;
import org.campus02.shape.ShapeA;
import org.campus02.shape.ShapeB;
import org.campus02.shape.ShapeC;
import org.campus02.shape.ShapeD;
import org.campus02.shape.ShapeE;
import org.campus02.shape.ShapeF;

public class DemoSnake {



	public static void main(String[] args) {
//		FrameShape frame= new FrameShape();
//		System.out.println(frame);
		
//		frame.moveUp();
//		frame.moveUp();frame.moveUp();frame.moveUp();frame.moveUp();frame.moveUp();frame.moveUp();frame.moveUp();frame.moveUp();
//		System.out.println(frame);
//		frame.moveLeft();frame.moveLeft();frame.moveLeft();frame.moveLeft();frame.moveLeft();frame.moveLeft();frame.moveLeft();
//		System.out.println(frame);
//		frame.moveDown();frame.moveDown();frame.moveDown();frame.moveDown();frame.moveDown();frame.moveDown();frame.moveDown();
//		System.out.println(frame);
//		frame.moveRight();frame.moveRight();frame.moveRight();frame.moveRight();frame.moveRight();frame.moveRight();frame.moveRight();
//		System.out.println(frame);
		
		
		
		Scanner scan = null;
		try {
		scan= new Scanner(System.in);
		
		while(true)
		{
			
			int nbr= 0;
			while(nbr!=5 && nbr!=2 && nbr!=4 && nbr!=8 && nbr!=6)
			{
				System.out.println("Press 5 to (re)start \n give a move code 2, 4, 6 or 8");
				nbr = scan.nextInt();
			}
			
			
			switch (nbr) {
			//press 5 to start
			case 5:
				FrameShape snake= new FrameShape();
				System.out.println(snake);
				System.out.println(snake.getX());
				System.out.println(snake.getY());
				break;
			case 2:
				FrameShape snakeDown= new FrameShape();
				snakeDown.moveDown();
				System.out.println(snakeDown);
				System.out.println(snakeDown.getX());
				System.out.println(snakeDown.getY());
				break;
			case 4:
				FrameShape snakeLeft= new FrameShape();
				snakeLeft.moveLeft();
				System.out.println(snakeLeft);
				System.out.println(snakeLeft.getX());
				System.out.println(snakeLeft.getY());
				break;
			case 6:
				FrameShape snakeRight= new FrameShape();
				snakeRight.moveRight();
				System.out.println(snakeRight);
				System.out.println(snakeRight.getX());
				System.out.println(snakeRight.getY());
				break;
			case 8:
				FrameShape snakeUp= new FrameShape();
				snakeUp.moveUp();
				System.out.println(snakeUp);	
				System.out.println(snakeUp.getX());
				System.out.println(snakeUp.getY());
				break;
			default:
				break;
			}
		}
		}
		finally {
			if (scan != null)
			{
				scan.close();
			}
		}
		
	

	}

}
